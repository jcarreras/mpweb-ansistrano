# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Práctica para aprender a usar Ansistrano.

# Consideraciones
-----------------------

Ansible y el rol Ansistrano se puede usar instalándose en la máquina host y ejecutándolo desde la CLI directamente. Aún así, hacerlo mediante el uso de Containers aseguramos tener un entorno reproducible, que fácilmente podrá ser usado por un sistema de Continuous Delivery y simplifica la tarea de mantenimiento del entorno.

La presuposición es que en producción no tenemos un sistema de orquestración de containers, dado que de otra forma contruiríamos una nueva imagen de Docker con el nuevo código y usaríamos las herramientas propias del sistema de orquestración para subir el código.

En éste caso supondremos que tenemos servidores físicos o virtuales, con acceso por SSH a los mismos, con un Apache instalado y ya sirviendo una página web.


# Entorno
-----------------------

Para realizar esta práctica se necesita un servidor donde subir el código. Se recomienda crear una máquina EC2.

* Tipo de instancia: t2.micro
* Configuración de Security Groups: Puertos 22 y 80 abiertos al mundo (0.0.0.0/0)
* IP: Publica (EIP). Apunta la IP. La vas a necesitar en los siguientes pasos.
* Imagen: Ubuntu 18.04



# Configuración del entorno
--------------------------------

Primero necesitamos configurar la IP Publica del servidor modificando el fichero ./provision/inventory. Cambia la IP con la de tu máquina EC2. En mi caso:

# ![screenshot](.inventory.png)

Para no tener que instalar Apache manualmente, se ha creado un playbook de Ansible que instala y configura Apache. Ésto se sale de la practica en si mismo, y facilita un entorno al estudiante donde practicar los pasos a producción. Para provisionar la máquina EC2 sigue los siguienes pasos:

- Descargamos una imagen de Docker con ansible y ejecutamos una shell donde tenemos el comando de Ansible disponible
- `docker run --rm -it -v $(pwd):/ansible -v ~/.ssh/id_rsa:/root/.ssh/id_rsa willhallonline/ansible:2.8-stretch /bin/sh`
- Nótese que `~/.ssh/id_rsa` debe ser la clave privada con la que nos podemos conectar a la máquina EC2. Cambia el path si lo necesitas.
- Ejecuta el siguiente comando para instalar Apache y configurarlo:
-  `ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i provision/inventory provision/playbook.yml`
- El output del comando debería ser similar a:

# ![screenshot](.install_apache.png)

Si ahora abrimos el navegador y pones tu IP de la máquina EC2 deberías ver algo así como:

# ![screenshot](.apache_404.png)

Son buenas noticias! Significa que en el servidor remoto hay instalado un servidor Web, pero que todavía no tenemos contenido que servir. El entorno ha quedado preparado.


# Instrucciones para realizar la práctica
-------------------------------------------

- Mira el contenido del fichero `deploy/deploy.yml` para entender la configuración que se ha hecho para hacer los pasos a producción.
- En la shell que tienes abierta dentro del container de Ansible...
- ... ejecuta el siguiente comando: `ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i provision/inventory deploy/deploy.yml`
- Entra en `http://TU.IP.PUBLICA.AQUI/` en tu navegador
- Verás que se ha hecho paso a producción del contenido de /src
- Cambia el fichero ./src/index.html, por ejemplo el tag `<h1>`
- Ejecuta otra vez un paso a producción con: `ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i provision/inventory deploy/deploy.yml`
- Mira que en `http://TU.IP.PUBLICA.AQUI/` la web haya cambiado con los cambios que hayas hecho
- Entra en la máquina de EC2 y mira el directorio `/var/www/html/` y analiza el contenido del directorio


# Desinstalación
-----------------------

- Con salir del container donde ejecutamos Ansible/Ansistrano es suficiente
- Puedes borrar la imagen `willhallonline/ansible:2.7` de docker si consideras
- Al acabar la práctica, borra manualmente la máquina de EC2. Éste paso es importante ya que las máquinas de EC2 consumen créditos.
